import { add, substract, division, multiply } from './Maths.js';

export class Element{

  constructor(){
      this._DOMElement = null;
      this.initDOMElement();
      this._a = null;
      this._b = null;
      this._operator = null;
  }

  get a(){
    return this._a;
  }

  set a(value){
    this._a = value;
  }

  get result(){
    return this._result;
  }

  set result(value){
    this._result = value;
    this.DOMElement.querySelector(".result").innerHTML = value;
  }

  get b(){
    return this._b;
  }

  set b(value){
    this._b = value;
  }

  get operator(){
    return this._operator;
  }

  set operator(value){
    this._operator = value;
  }

  get DOMElement(){
    return this._DOMElement;
  }

  set DOMElement(value){
    this._DOMElement = value;
  }

  initDOMElement(){
    var a = document.createElement("input");
    a.type = "number";
    a.placeholder = "a";
    a.onkeyup = (event) => {
      this.dataBind(event,"a");
      this.checkToResolve();
    }

    var b = document.createElement("input");
    b.type = "number";
    b.placeholder = "b";
    b.onkeyup = (event) => {
      this.dataBind(event,"b");
      this.checkToResolve();
    }

    var operador = document.createElement("input");
    operador.placeholder = "+,-,*,/";
    operador.onkeyup = (event) => {
      this.dataBind(event,"operator");
      this.checkToResolve();
    }

    var result = document.createElement("div");
    result.classList.add("result");

    var el = document.createElement("div");
    el.classList.add("element");

    el.appendChild(a);
    el.appendChild(operador);
    el.appendChild(b);
    el.appendChild(result);

    this.DOMElement = el;
  }

  dataBind(e,variable){
    this[variable] = e.target.value;
    console.log(`${variable} => ${this[variable]}`);
  }

  checkToResolve(){
    console.log("cheking");
    console.log(`${this.a} ${this.operator} ${this.b}`)
    if(this.a != null && this.b != null && this.operator != null){
      console.log("ready to operate");
      switch (this.operator) {
        case "+":
            this.result = add(this.a,this.b);
          break;
        default:

      }
    }
  }

}
